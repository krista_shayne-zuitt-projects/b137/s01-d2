package com.zuitt.s01d2;

public class DataTypesAndVariables {
    public static void main(String[] args) {
        // Variable declaration with initialization
        int firstNumber = 0;

        // Variable declaration without initialization
        int secondNumber;

        secondNumber = 23; //Initialization after declaration

        // Concatenation
        System.out.println("The value of the first number: " + firstNumber);
        System.out.println("The value of the second number: " + secondNumber);

        //Mini-activity:

        //1. Create a variable to contain an age value
        int age;
        age = 21;

        //2. Create a variable to contain a human weight
        //double weight;
        float weight;
        weight= 50;

        //3. Create a variable to contain a human gender
        char gender;
        gender = 'M';

        //4. Create a variable to contain a civil status if it is single
        boolean isSingle = true;

        System.out.println("Age is: " + age);
        System.out.println("Weight is: " + weight);
        System.out.println("Gender is: " + gender);
        System.out.println("Single is: " + isSingle);

        // Non-primitives:
        // Create a string variable for a name
        // <data_types> <identifier> = "value";

        String name = "Juan Dela Cruz";
        String concatenatedName = "Juan" + "Dela" + "Cruz";

        System.out.println("name:" +name);
        System.out.println("concatenatedName:" + concatenatedName);



    }
}
